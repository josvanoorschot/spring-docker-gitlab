package com.bullsoft.mywebappy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MywebappyApplication {

	public static void main(String[] args) {
		SpringApplication.run(MywebappyApplication.class, args);
	}

}
